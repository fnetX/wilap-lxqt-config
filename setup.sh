#!/bin/bash
# common setup script

for script in scripts/*; do
	[ -f "$script" ] && source "$script"
	# sudo -k # disabled for convenience: exit sudo so no script that doesn't need sudo can get these privileges
done
echo "Done."
