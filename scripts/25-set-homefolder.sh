filenames_with_home[0]='.config/qterminal.org/qterminal.ini'
filenames_with_home[1]='.config/pcmanfm-qt/lxqt/settings.conf'
for conffile in "${filenames_with_home[@]}"
do
	envsubst \$HOME > $HOME/$conffile < config/$conffile
done
