read -p "Do you want to install firefox policies? [y/n] " -n 1 -r
echo 
if [[ $REPLY =~ ^[Yy]$ ]]
then
	sudo cp -r config/firefox/* /usr/share/firefox-esr
	sudo mkdir -p /etc/firefox/policies
	sudo cp config/firefox/distribution/policies.json /etc/firefox/policies/
	sudo cp -r config/firefox/distribution/searchplugins/ /etc/firefox/
fi
