if [[ -d "$HOME/.config" ]]
then
	echo "Moving old configuration to ~/.config-default"
	mv $HOME/.config $HOME/.config-default
fi
if [[ -d "config/.config" ]]
then
	cp -r config/.config $HOME/
fi

